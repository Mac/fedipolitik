# Fedipolitik: Allgemeine Infos

Diese Datei beinhaltet eine Auflistung von Fediverse-Profilen politischer Entitäten (Parteien, Mandatsträger:innen, Amtsträger:innen, Behörden, offizielle Kanditat:innen für Wahlen, "wichtige Einzelpersonen").

Die Datei kann und soll kollektiv erweitert und aktualisiert werden. Möglichkeiten:

- Pull-Request
- Issue
- Nachricht an aktuelle(n) Maintainer:
  - https://social.tchncs.de/@cark
- Antwort auf: https://social.tchncs.de/@cark/105650620879066525 (Original-Toot).

Anspruch auf Vollständigkeit [uder](https://cknoll.github.io/uder) Korrektheit wird explizit negiert. Eine Erwähnung hier ist nicht gleichzusetzen mit inhaltlicher oder formaler Zustimmung oder Ablehnung. Sortiert wird innerhab der einzelnen Abschnitte grundsätzlich alphabetisch.

Diese Datei ist unter einer Creative Commons [CC-BY-SA 4.0 Lizenz](https://creativecommons.org/licenses/by-sa/4.0/legalcode.de) für die Weiterverbreitung und Nutzung freigegeben.

---

# Politische Fediverse-Profile

## Parteien und Untergliederungen

### Union

#### Christlich Demokratische Union Deutschlands (CDU)

| Wer                        | Link                                |
| :------------------------- | :---------------------------------- |
| CDU-Bezirksverband Kappeln | https://mastodon.social/@cdukappeln |

#### Christlich-Soziale Union (CSU)

Zum jetzigen Zeitpunkt sind keine Fediverse-Präsenzen der CSU oder ihrer Gliederungen bekannt.

### Sozialdemokratische Partei Deutschlands (SPD)

| Instanzen        | Link                   |
| :--------------- | :--------------------- |
| Mastodon-Instanz | https://sozen.network/ |

| Wer o. Was?                 | Link                                      |
| :-------------------------- | :---------------------------------------- |
| SPD Dudweiler               | https://sozen.network/@SPD_Dudweiler      |
| SPD-Bundesvorstand `(Bot)`  | https://sozen.network/@bundesvorstand_bot |
| SPD-Fraktion Bayern `(Bot)` | https://sozen.network/@spdfraktion_bayern |

### Freie Demokratische Partei Deutschlands (FDP)

Zum jetzigen Zeitpunkt sind keine Fediverse-Präsenzen der FDP oder ihrer Gliederungen bekannt.

### BÜNDNIS 90/DIE GRÜNEN (B'90/Grüne)

| Instanzen         | Link                                |
| :---------------- | :---------------------------------- |
| Mastodon-Instanz  | https://gruene.social/              |
| Pixelfed-Instanz  | https://pixel.gruene.social/        |
| Mobilizon-Instanz | https://events.gruene.social/       |
| PeerTube-Instanz  | https://peertube.netzbegruenung.de/ |

| Wer o. Was?                                                       | Link                                        |
| :---------------------------------------------------------------- | :------------------------------------------ |
| BÜNDNIS 90/DIE GRÜNEN Bonn: Arbeitskreis Digitaler Wandel         | https://bonn.social/@AKDiWa                 |
| BÜNDNIS 90/DIE GRÜNEN Kreisverband Treptow-Köpenick               | https://gruene.social/@gruene_tk            |
| BÜNDNIS 90/DIE GRÜNEN Ortsverband München Hadern                  | https://gruene.social/@gruenemuenchenhadern |
| BÜNDNIS 90/DIE GRÜNEN Kreisverband Börde (Sachsen-Anhalt)         | https://gruene.social/@boerde               |
| BÜNDNIS 90/DIE GRÜNEN in der Samtgemeinde Harsefeld               | https://gruene.social/@harsefeld            |
| BÜNDNIS 90/DIE GRÜNEN im Bezirksausschuss 3 (München) Maxvorstadt | https://gruene.social/@maxvorstadt          |
| BÜNDNIS 90/DIE GRÜNEN Stadthagen                                  | https://gruene.social/@Stadthagen           |
| BÜNDNIS 90/DIE GRÜNEN Urbach                                      | https://gruene.social/@urbach               |

### DIE LINKE

| Wer o. Was?                                                  | Link                               |
| :----------------------------------------------------------- | :--------------------------------- |
| Linksfraktion in der Bezirksverodnetenversammlung von Pankow | https://mastodon.social/@linkebvvp |

### Die PARTEI

| Instanzen        | Link                      |
| :--------------- | :------------------------ |
| Mastodon-Instanz | https://die-partei.social |

| Wer o. Was?                       | Link                                     |
| :-------------------------------- | :--------------------------------------- |
| Die Partei Hannover               | https://hannover.social/@diepartei       |
| Die Partei OV Kronshagen          | https://norden.social/@PARTEI_Kronshagen |
| Die Partei KV Münster             | https://mastodon.social/@Die_PARTEI_MS   |
| Die Partei KV Nordwestmecklenburg | https://die-partei.social/@diePARTEI_NWM |
| Die Partei Pinneberg              | https://die-partei.social/@diepartei_pi  |
| Die Partei Reutlingen             | https://die-partei.social/@DiePARTEI_RT  |
| Die Partei Steiermark             | https://graz.social/@diePARTEI_stmk      |
| Die Partei Wien                   | https://wien.rocks/@diePARTEI            |

### Piratenpartei

| Instanzen         | Link               |
| :---------------- | :----------------- |
| Friendica-Instanz | https://pirati.ca/ |

| Wer o. Was?                                                           | Link                                                                  |
| :-------------------------------------------------------------------- | :-------------------------------------------------------------------- |
| Piratenpartei Bonn                                                    | https://bonn.social/@piratenbonn                                      |
| Piratenpartei Baden-Württemberg (BaWü / BW)                           | https://mastodon.cloud/@PiratenBW                                     |
| Piratenpartei Bund (inaktiv)                                          | https://mastodon.partipirate.org/@Piratenpartei                       |
| Piratenpartei Dresden                                                 | https://pirati.ca/profile/piratendresden                              |
| Piratenpartei Dresden (PeerTube)                                      | https://video.dresden.network/accounts/piraten_dresden/video-channels |
| Piratenpartei gem. Kreisverband der Kreise Main-Taunus und Hochtaunus | https://mastodon.social/@taunuspiraten                                |
| Piratenpartei Ortsverband Dresden-Neustadt                            | https://dresden.network/@neustadtpiraten                              |
| Piratenpartei Rhein-Erft                                              | https://pirati.ca/profile/piratenrheinerft                            |
| Piratenpartei Saarland                                                | https://mastodon.social/@piraten_saar                                 |
| Piratige Hochschulgruppe in Dresden                                   | https://chaos.social/@hopidd                                          |

### Sonstige

| Wer o. Was?   | Link                                        |
| :------------ | :------------------------------------------ |
| Die Sonstigen | https://climatejustice.social/@diesonstigen |

### DiEM25

|  Wer o. Was?  | Link                                  |
| :-----------: | :------------------------------------ |
| DiEM25 Berlin | https://eupublic.social/@diem25berlin |

### Klimaliste

| Wer o. Was?                  | Link                                                  |
| :--------------------------- | :---------------------------------------------------- |
| Klimaliste Baden-Württemberg | https://climatejustice.social/@KlimalisteBW           |
| Klimaliste Bundespartei      | https://climatejustice.social/@Klimaliste_Deutschland |
| Klimafreunde Köln            | https://social.tchncs.de/@klima_freunde               |
| Klimaliste Leverkusen        | https://climatejustice.social/@klimalisteLEV          |
| Klimaliste Rems-Murr-Kreis   | https://climatejustice.social/@klimaliste_rmk         |
| Klimaliste Sachsen-Anhalt    | https://climatejustice.social/@KlimalisteST           |

## Politische Jugendorganisationen

### Grüne Jugend

| Wer o. Was?                              | Link                                  |
| :--------------------------------------- | :------------------------------------ |
| Grüne Jugend Netzpolitik                 | https://gruene.social/@gjnetzpolitik  |
| Grüne Jugend Brandenburg                 | https://gruene.social/@gj_bb          |
| Grüne Jugend Darmstadt/Darmstadt-Dieburg | https://gruene.social/@gjdadi         |
| Grüne Jugend Karlsruhe                   | https://social.tchncs.de/@GJKarlsruhe |

## Behörden, Ämter und Parlamente

### Bundesbehörden

| Instanzen        | Link                    |
| :--------------- | :---------------------- |
| Mastodon-Instanz | https://social.bund.de/ |

| Wer o. Was?                                                                             | Link                                    |
| :-------------------------------------------------------------------------------------- | :-------------------------------------- |
| Bundesbeauftragte/r für den Datenschutz und die Informationsfreiheit (BfDI)             | https://social.bund.de/@bfdi            |
| Bundesamt für Sicherheit in der Informationstechnik (BSI)                               | https://social.bund.de/@bsi             |
| Stiftung Datenschutz                                                                    | https://social.bund.de/@DS_Stiftung     |
| Föderale IT-Kooperation                                                                 | https://social.bund.de/@FITKOfoederal   |
| Konferenz der unabhängigen Datenschutzaufsichtsbehörden des Bundes und der Länder (DSK) | https://social.bund.de/@dsk             |
| Presse- und Informationsamt der Bundesregierung (Bundespresseamt; BPA)                  | https://social.bund.de/@Bundespresseamt |
| Bundesministerium für Bildung und Forschung (BMBF)                                      | https://social.bund.de/@bmbf_bund       |
| Informationstechnikzentrum Bund (ITZBund)                                               | https://social.bund.de/@itzbund         |

#### inoffiziell

| Wer o. Was?                                                                                                              | Link                                  |
| :----------------------------------------------------------------------------------------------------------------------- | :------------------------------------ |
| FediNINA - Informationen des Bundesamtes für Bevölkerungsschutz und Katastrophenhilfe im Fediverse `(Inoffizielle Bots)` | https://social.prepedia.org/@FediNINA |

### Berlin

| Wer o. Was?                                                            | Link                           |
| :--------------------------------------------------------------------- | :----------------------------- |
| Berliner Beauftragte für Datenschutz und Informationsfreiheit (BlnBDI) | https://social.bund.de/@BlnBDI |

### Baden-Württemberg

| Instanzen        | Link                 |
| :--------------- | :------------------- |
| Mastodon-Instanz | https://bawü.social/ |

| Wer o. Was?                                                                          | Link                                   |
| :----------------------------------------------------------------------------------- | :------------------------------------- |
| Landesregierung des Landes Baden-Württemberg (LReg BW / BaWü)                        | https://mastodon.social/@RegierungBW   |
| Ministerium für Umwelt, Klima und Energiewirtschaft Baden-Württemberg (UM BaWü / BW) | https://bawü.social/@Umweltministerium |
| Regierungspräsidium Freiburg                                                         | https://bawü.social/@RPFreiburg        |
| Landesbeauftragte/r für den Datenschutz und die Informationsfreiheit (LfDI BaWü)     | https://bawü.social/@lfdi              |
| Landeszentrale für politische Bildung Baden-Württemberg (LpB BW / BaWü)              | https://dizl.de/@lpb                   |

### Rheinland-Pfalz

| Wer o. Was?             | Link                          |
| :---------------------- | :---------------------------- |
| Landtag Rheinland-Pfalz | https://social.bund.de/@ltrlp |

## Amtsträger:innen

| Wer                        | Link                              |
| :------------------------- | :-------------------------------- |
| Prof. Ulrich Kelber (BfDI) | https://bonn.social/@ulrichkelber |

## Mandatsträger:innen

| Mitglieder des Europäischen Parlaments (deutschsprachig) | Link                               |
| :------------------------------------------------------- | :--------------------------------- |
| Patrick Breyer                                           | https://chaos.social/@echo_pbreyer |

| Mitglieder des Bundestages | Link                                  |
| :------------------------- | :------------------------------------ |
| Maik Außendorf             | https://gruene.social/@AussenMa       |
| Konstantin von Notz        | https://gruene.social/@KonstantinNotz |
| Tabea Rößner               | https://gruene.social/@TabeaRoessner  |
| Margit Stumpp              | https://gruene.social/@MargitStumpp   |

| Mitglieder eines Landtages | Link                                   |
| :------------------------- | :------------------------------------- |
| Marie Schäffer             | https://gruene.social/@marie_schaeffer |

| Mitglieder/Fraktionen eines Kommunalparlamentes             | Link                                   |
| :---------------------------------------------------------- | :------------------------------------- |
| Stefan Borggraefe                                           | https://mastodon.social/@BorgTenOfNine |
| Daniel Gaittet (Stadtrat Regensburg, BÜNDNIS 90/DIE GRÜNEN) | https://gruene.social/@dgaittet        |
| DISSIDENTEN-Fraktion im Dresdener Stadtrat                  | https://dresden.network/@dissidentenDD |
| Lukas Weidinger (Stadtrat Würzburg, BÜNDNIS 90/DIE GRÜNEN)  | https://gruene.social/@lukasweidinger  |
| Dr. Martin Schulte-Wissermann (Stadtrat Dresden, Piraten)   | https://dresden.network/@mswdresden    |

## Weitere Personen des politischen Lebens

| Wer                       | Relevanz                         | Link                              |
| :------------------------ | :------------------------------- | :-------------------------------- |
| Stephanie Henkel (ÜckÜck) | Bundestagswahlkandidatin 2021    | https://dresden.network/@ueckueck |
| Malte Spitz               | Parteirat Bündnis '90/Die Grünen | https://gruene.social/@maltespitz |

## Parteigründungsinitiativen

| Wer                             | Link                      |
| :------------------------------ | :------------------------ |
| Partei für Lebensqualität (PLQ) | https://legal.social/@plq |

## Überregionale Nichtregierungsorganisationen & Projekte

| Wer                                              | Link                                              |
| :----------------------------------------------- | :------------------------------------------------ |
| Amnesty Digital DE                               | https://social.tchncs.de/@amnesty_digital_de      |
| Anoxinon e. V.                                   | https://social.anoxinon.de/@Anoxinon              |
| Bedingungsloses Grundeinkommen (BGE/UBI) Fanclub | https://social.tchncs.de/@BGE_Fanclub             |
| BUNDjugend `(Bot)`                               | https://botsin.space/@BUNDjugend                  |
| BUNDjugend (Arbeitskreis Digitalisierung)        | https://climatejustice.global/@BUNDjugend_ak_digi |
| Computertruhe e. V.                              | https://mastodon.social/@computertruhe            |
| Computertruhe e. V. (Standort Berlin)            | https://mastodon.social/@ComputertruheBerlin      |
| Computertruhe e. V. (Standort Chemnitz)          | https://chaos.social/@ComputertruheChemnitz       |
| Computertruhe e. V. (Standort München)           | https://social.tchncs.de/@ComputertruheMuenchen   |
| Computerwerk Darmstadt e. V.                     | https://chaos.social/@computerwerk                |
| Digitalcourage e. V.                             | https://digitalcourage.social/@digitalcourage     |
| Ende Gelände                                     | https://climatejustice.global/@ende_gelaende      |
| Fridays for Future                               | https://chaos.social/@fff                         |
| Frag den Staat                                   | https://chaos.social/@fragdenstaat                |
| Free Software Foundation Europe                  | https://mastodon.social/@fsfe                     |
| Greenpeace `(Bot)`                               | https://newsbots.eu/@Greenpeace                   |
| Liberapay                                        | https://mastodon.xyz/@Liberapay                   |
| Linux User im Bereich der Kirchen e. V. (LUKI)   | https://kirche.social/@luki                       |
| Mobilsicher.de                                   | https://mastodontech.de/@mobilsicher              |
| NOYB - Europäisches Zentrum für digitale Rechte  | https://mastodon.social/@noybeu                   |
| Parents4Future                                   | https://climatejustice.global/@parents4future     |
| Privacy is not a Crime                           | https://mastodon.online/@PrivacyIsNotACrime       |
| Prototype Fund                                   | https://mastodon.social/@PrototypeFund            |
| Senfcall                                         | https://chaos.social/@senfcall                    |
| XR Germany                                       | https://social.rebellion.global/@xrgermany        |
